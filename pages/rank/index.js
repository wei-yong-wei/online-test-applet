//index.js
//获取应用实例
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    selectQuestionMenu: {},
    objectQuestionMenu: {},
    questionMenu: [],
    index: 0,
    spinShow: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //获取套题
     var that = this;
     that.setData({
      spinShow: true
    });
      app.formPost('/api/wx/student/exampaper/answer/rankList', null).then(res => {
        
        that.setData({
          spinShow: false,
         questionMenu: res.response
         })
         console.log(that.data.questionMenu);
      })     
  },
  /**
   * 选择题库
   */
  changeMenu(e) {
    var that = this;
    console.log( e.detail.value);
    that.setData({
      index: e.detail.value,
      selectQuestionMenu: that.data.questionMenu[e.detail.value],
    })
    console.log( that.data.selectQuestionMenu);
    // var objectQuestionMenu = this.data.objectQuestionMenu
    var examPaperId = that.data.selectQuestionMenu.examPaperId;
    console.log(examPaperId);
    app.formPost('/api/wx/student/exampaper/answer/rankListOne'+'/'+examPaperId,null).then(res => {
      if(res.response){
        wx.navigateTo({
          url: '/pages/rankList/index?examPaperId=' + examPaperId,
        })
      }else{
        wx.showToast({
          title: '暂无记录',
          duration: 1500,
          image: '/images/warning.png'
        })
        return;
      }
    })
  },
})