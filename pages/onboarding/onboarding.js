// miniprogram/pages/onboarding/onboarding.js
Page({
  data: {
    step: 1,
  },
  onLoad() {
    // 每一位用户，都需要在onboarding的页面上触发新增用户到USERS表
    // const isFir=wx.getStorageSync('isFirst')
    // if(isFir==true){
    //   wx.redirectTo({
    //     url: '/pages/NewWelcome/index',
    //   })
    // }else{

    // }
  },
  next() {
    const { step } = this.data
    if (Number(step) === 6) {
      wx.redirectTo({
        url: '/pages/NewWelcome/index',
      })
      // wx.setStorageSync('isOnboarding', 'v3.5.0')
    } else {
      wx.vibrateShort()
      this.setData({
        step: step + 1,
      })
    }
  },
})
