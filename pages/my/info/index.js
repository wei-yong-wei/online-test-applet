// pages/user/info/index.js
const app = getApp()
Page({
  data: {
    userInfo: null,
    spinShow: false,
    levelIndex: 0
  },
  onLoad: function (options) {
    this.loadUserInfo()
  },
  loadUserInfo() {
    let _this = this
    _this.setData({
      spinShow: true
    }); 
        _this.setData({
          userInfo: app.globalData.userInfo,
          levelIndex: app.globalData.userInfo.userLevel,
          deptIndex: app.globalData.userInfo.userDept
        });
      _this.setData({
        spinShow: false
      });
  },
  bindLevelChange: function (e) {
    this.setData({
      levelIndex: e.detail.value
    })
  },
  bindDeptChange: function (e) {
    this.setData({
      deptIndex: e.detail.value
    })
  },
  bindDateChange(e) {
    let {
      value
    } = e.detail;
    this.setData({
      "userInfo.birthDay": value
    })
  },
  formSubmit: function (e) {
    let _this = this
    wx.showLoading({
      title: '提交中',
      mask: true
    })
    app.formPost('/api/wx/student/user/update', e.detail.value)
      .then(res => {
        if (res.code == 1) {
          wx.reLaunch({
            url: '/pages/my/index/index',
          });
        } else {
          app.message(res.message, 'error')
        }
        wx.hideLoading()
      }).catch(e => {
        app.message(e, 'error')
        wx.hideLoading()
      })
  }
})