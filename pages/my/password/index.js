// pages/user/info/index.js
const app = getApp()
Page({
  data: {
    userInfo: null,
    newPassword: null,
    newPassword1: null,
    spinShow: false,
    user: {
      id: 0,
      age: 0,
      realName: null,
      phone: null,
      password: null
    },
  },
  onLoad: function () {
    this.loadUserInfo()
  },
  loadUserInfo() {
    let _this = this
    _this.setData({
      spinShow: true
    });
    app.formPost('/api/wx/student/user/current', null).then(res => {
      if (res.code == 1) {
        _this.setData({
          userInfo: res.response,
          'user.id': res.response.id,
          'user.age': res.response.age,
          'user.realName': res.response.realName,
          'user.password': this.data.newPassword,
          'user.phone': res.response.phone
        });
      }
      _this.setData({
        spinShow: false
      });
    }).catch(e => {
      _this.setData({
        spinShow: false
      });
      app.message(e, 'error')
    })
  },
  formSubmit: function (e) {
    let _this = this
    console.log(e)
    if (e.detail.value.newPassword.length < 6 || e.detail.value.newPassword1.length < 6) {
      app.message("密码长度不够6位，请重新输入！", 'error')
      wx.showModal({
        title: '提示',
        content: '密码长度不够6位，请重新输入！'
      })
    };
    if (e.detail.value.newPassword != e.detail.value.newPassword1) {
      app.message("两次输入密码不一致！", 'error')
      wx.showModal({
        title: '提示',
        content: '两次输入密码不一致，请重新输入！'
      })
    };
    if (e.detail.value.newPassword == e.detail.value.newPassword1 && e.detail.value.newPassword.length >= 6) {
      _this.setData({
        'user.password': e.detail.value.newPassword1
      });
      wx.showLoading({
        title: '提交中',
        mask: true
      })
      app.formPost('/api/wx/student/user/update', _this.data.user)
        .then(res => {
          if (res.code == 1) {
            wx.reLaunch({
              url: '/pages/my/index/index',
            });
          } else {
            app.message(res.message, 'error')
          }
          wx.hideLoading()
        }).catch(e => {
          app.message(e, 'error')
          wx.hideLoading()
        })
    }
  }
})