// pages/Home/home/home.js

const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    spinShow: false,
    swiperList: [{
      id: 0,
      type: 'image',
      url: '../../images/logo.jpg'
    }, {
      id: 1,
      type: 'image',
      url: 'https://light-year-1258515630.cos.ap-guangzhou.myqcloud.com/%E5%B0%8F%E9%A2%98%E5%BA%93Lite/2.png',
    }],
    elements: [{
        title: '班级管理',
        name: 'ClassesList',
        color: 'grey',
        icon: 'squarecheckfill',
        url: "/pages/classesList/index",
        time: '0.1s'
      },
      {
        title: '考试分析',
        name: 'Examination',
        color: 'blue',
        icon: 'add',
        url: "/pages/history/index",
        time: '0.2s'
      },
      {
        title: '我的成绩单',
        name: 'Report card',
        color: 'pink',
        icon: 'copy',
        url: "/pages/reportList/index",
        time: '0.3s'
      },
      {
        title: '我的证书',
        name: 'My certificate',
        color: 'orange',
        icon: 'vipcard',
        url: "/pages/certificateList/index",
        time: '0.4s'
      },
      {
        title: '地图定位',
        name: 'Map',
        color: 'brown',
        icon: 'loading2',
        url: "/pages/map/index",
        time: '0.5s'
      },
      {
        title: '使用介绍',
        name: 'Introduction',
        color: 'green',
        icon: 'messagefill',
        url: "/pages/introduce/index",
        time: '0.6s'
      },
      {
        title: '考试日历',
        name: 'Calendar',
        color: 'purple',
        icon: 'font',
        url: "/pages/calendar/calendar",
        time: '0.7s'
      },
      {
        title: '便捷网站',
        name: 'Web',
        color: 'cyan',
        icon: 'newsfill',
        url: "/pages/web/web",
        time: '0.8s'
      },
      {
        title: '报价文件',
        name: 'QuotedPrice',
        color: 'olive',
        icon: 'copy',
        url: "/pages/cert/cert",
        time: '0.9s'
      },
      {
        title: '培训资料',
        name: 'Train',
        color: 'red',
        icon: 'myfill',
        url: "/pages/book/book",
        time: '1.0s'
      },
      {
        title: '成绩查询',
        name: 'ScoreInquiry',
        color: 'yellow',
        icon: 'mark',
        url: "/pages/search/search",
        time: '1.1s'
      },
      {
        title: '百问百答',
        name: 'Q&A',
        color: 'cyan',
        icon: 'questionfill',
        url: "/pages/wenDa/wenDa",
        time: '1.2s'
      },
      {
        title: '即将上线 ',
        name: 'coming',
        color: 'mauve',
        icon: 'discover',
        time: '1.3s'
      }
    ]


  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let _this = this
    if (app.globalData.userInfo.role === 3)
      _this.setData({
        elements: [{
          title: '班级管理',
          name: 'ClassesList',
          color: 'grey',
          icon: 'squarecheckfill',
          url: "/pages/classesList/index",
          time: '0.1s'
        },
          {
            title: '成绩排名',
            name: 'Ranking',
            color: 'cyan',
            icon: 'edit',
            url: "/pages/rank/index",
            time: '0.2s'
          },
          {
            title: '我的成绩单',
            name: 'Report card',
            color: 'pink',
            icon: 'copy',
            url: "/pages/reportList/index",
            time: '0.3s'
          },
          {
            title: '我的证书',
            name: 'My certificate',
            color: 'orange',
            icon: 'vipcard',
            url: "/pages/certificateList/index",
            time: '0.4s'
          },
          {
            title: '考试分析',
            name: 'Examination',
            color: 'blue',
            icon: 'add',
            url: "/pages/history/index",
            time: '0.5s'
          },
          {
            title: '图表分析',
            name: 'Echarts',
            color: 'olive',
            icon: 'colorlens',
            url: "/pages/echarts/index",
            time: '0.6s'
          },
          {
            title: '地图定位',
            name: 'Map',
            color: 'brown',
            icon: 'loading2',
            url: "/pages/map/index",
            time: '0.7s'
          },
          {
            title: '使用介绍',
            name: 'Introduction',
            color: 'green',
            icon: 'messagefill',
            url: "/pages/introduce/index",
            time: '0.8s'
          },
          {
            title: '考试日历',
            name: 'Calendar',
            color: 'purple',
            icon: 'font',
            url: "/pages/calendar/calendar",
            time: '0.9s'
          },
          {
            title: '便捷网站',
            name: 'Web',
            color: 'cyan',
            icon: 'newsfill',
            url: "/pages/web/web",
            time: '1.0s'
          },
          {
            title: '报价文件',
            name: 'QuotedPrice',
            color: 'olive',
            icon: 'copy',
            url: "/pages/cert/cert",
            time: '1.1s'
          },
          {
            title: '培训资料',
            name: 'Train',
            color: 'black',
            icon: 'myfill',
            url: "/pages/book/book",
            time: '1.2s'
          },
          {
            title: '成绩查询',
            name: 'ScoreInquiry',
            color: 'yellow',
            icon: 'mark',
            url: "/pages/search/search",
            time: '1.3s'
          },
          {
            title: '百问百答',
            name: 'Q&A',
            color: 'cyan',
            icon: 'questionfill',
            url: "/pages/wenDa/wenDa",
            time: '1.4s'
          },
          {
            title: '地图定位Pro',
            name: 'MapList',
            color: 'red',
            icon: 'loading',
            url: "/pages/mapList/index",
            time: '1.5s'
          },
          {
            title: '即将上线 ',
            name: 'coming',
            color: 'mauve',
            icon: 'discover',
            time: '1.6s'
          }
        ]
      });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },
  previewImage: function (e) {
    var current = e.target.dataset.src;
    wx.previewImage({
      current: current, // 当前显示图片的http链接  
      urls: this.data.cooperation_img // 需要预览的图片http链接列表  
    })
  },


  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})