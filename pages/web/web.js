// pages/school/web.js
Page({
  /**
   * 页面的初始数据
   */
  data: {
    isLoading: true,
    wan: [
      [{
        title: 'OA',
        background: 'https://6461-dabenben-jrvrn-1300136165.tcb.qcloud.la/OA.png?sign=fcc0972d9fa14f28f23d44d22a32d2d4&t=1621216540',
        url: ['https://oa.v3.shwpg.com/login.jsp']
      },
      {
        title: '智思云',
        background: 'https://6461-dabenben-jrvrn-1300136165.tcb.qcloud.la/ZSY.png?sign=684d90f9f2b3e30967b8cf640a959bf7&t=1621216517',
        url: ['https://www.zhisiyun.com/portal_v2/']
      }],
      [{
        title: 'CRM',
        background: 'https://6461-dabenben-jrvrn-1300136165.tcb.qcloud.la/CRM.png?sign=06a45fe35d29ce9d8843a415f843de8f&t=1621216559',
        url: ['https://cloud.hecom.cn/v1/login']
      }]
    ],
    lan: [
      [{
        title: 'processon',
        background: 'https://6461-dabenben-jrvrn-1300136165.tcb.qcloud.la/presson.png?sign=46a941f5239246afc1229083e3af6de4&t=1621217418',
        url: ['https://www.processon.com/']
      },
      {
        title: 'csdn',
        background: 'https://6461-dabenben-jrvrn-1300136165.tcb.qcloud.la/csdn.png?sign=50f3a8741d54dbe32b2d6bd935dc50f0&t=1621217454',
        url: ['https://www.csdn.net/']
      }],
      [{
        title: 'gitee',
        background: 'https://6461-dabenben-jrvrn-1300136165.tcb.qcloud.la/gitee.png?sign=063cf27ed5f8893ce6a9f418a2560380&t=1621217431',
        url: ['https://gitee.com/']
      },
      {
        title: '简书',
        background: 'https://6461-dabenben-jrvrn-1300136165.tcb.qcloud.la/js.png?sign=35e1d319f0975ddb4c6a24524dd44ca3&t=1621217402',
        url: ['https://www.jianshu.com/']
      }],
      [{
        title: '在线考试管理平台',
        background: 'https://6461-dabenben-jrvrn-1300136165.tcb.qcloud.la/exam.png?sign=c26552367b279580ad6bb2777ae58e84&t=1621217442',
        url: ['http://www.admin.dabenben.cn/']
      }, {
        title: 'minio',
        background: 'https://6461-dabenben-jrvrn-1300136165.tcb.qcloud.la/mimo.png?sign=e3dcb09abf244c902a383f1ac88c0095&t=1621217558',
        url: ['http://106.14.66.250:9000/']
      }]
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.inital();
  },
  inital: function () {
    wx.showShareMenu({
      withShareTicket: true,
      menus: ['shareAppMessage', 'shareTimeline']
    })
  },
  copyUrl(e) {
    const url = e.currentTarget.dataset.url

    wx.setClipboardData({
      data: url,
      success(res) {
        wx.getClipboardData({
          success(res) {
            console.log(res.data) // data
            wx.showToast({
              title: '复制成功,粘贴到浏览器访问',
              icon: 'none'
            })
          }
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    var that = this;
    setTimeout(function () {
      that.setData({
        isLoading: false
      });
    }, 400);
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: '“在线考试” - 便捷网站',
      path: 'pages/web/web'
    }
  }
})