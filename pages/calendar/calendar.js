// pages/school/calendar.js
var app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    calendarImage: 'https://upload-images.jianshu.io/upload_images/4697920-09baff817fe87d18.png',
    isLoading: true,
    screenHeight: '900',
    colorArr: ['red','orange','yellow','olive','green','cyan','blue','purple','mauve','pink','brown','grey','gray'],
    jsonContent: '',
    gradeArray: [{
      title: '任务试卷',
      value: '0'
    },{
      title: '固定试卷',
      value: '0'
    },{
      title: '时段试卷',
      value: '0'
    },{
      title: '班组试卷',
      value: '0'
    }],
    fixedPaper: [],
    pushPaper: [],
    timeLimitPaper: [],
    taskList: [],
    sjc:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.inital();
  },
  inital: function () {
    const device = wx.getSystemInfoSync();
    // console.log(device.screenHeight);
    this.setData({screenHeight: device.screenHeight});
    // 再通过setData更改Page()里面的data，动态更新页面的数据  
    var _this = this;
        // console.log(res.data)
        let dateObj = new Date()
      let year = dateObj.getFullYear()
      let month = dateObj.getMonth()
      let day = dateObj.getDate()
      let now = new Date()
      console.log(now)
      let target = new Date()  // 目标日期
      target.setYear(2021)
      target.setMonth(0)
      target.setDate(1)

      target.setHours(0)
      target.setMinutes(0)
      target.setSeconds(0)
      console.log(target)

      // 先保存起来，后续还会用
      let nowtime = now.getTime()    // 当前日期时间戳
      let targettime = target.getTime()    // 当前日期时间戳
      let sjc = parseInt((nowtime - targettime)/ 1000 / 60/60 /24 ) // 时间差
      console.log(sjc)
        _this.setData({ 
          fixedPaper: app.globalData.fixedPaper,
          pushPaper: app.globalData.pushPaper,
          timeLimitPaper: app.globalData.timeLimitPaper,
          taskList: app.globalData.taskList,
          sjc:sjc,
          "gradeArray[0].value":app.globalData.taskPaperCount,
          "gradeArray[1].value":app.globalData.fixedPaperCount,
          "gradeArray[2].value":app.globalData.timeLimitPaperCount,
          "gradeArray[3].value":app.globalData.pushPaperCount,
        })
        setTimeout(function () {
          _this.setData({ isLoading: false })
          wx.vibrateShort({ type: 'medium' })
        }, 800)

    wx.showShareMenu({
      withShareTicket: true,
      menus: ['shareAppMessage', 'shareTimeline']
    })
  },
  /**
  * 用户点击右上角分享
  */
  onShareAppMessage: function (res) {
    return {
      title: '北京科技大学天津学院校历',
      path: 'pages/school/calendar',
    }
  },
  showPic: function () {
    var image = this.data.calendarImage;
    wx.previewImage({ current: image, urls: [image] })
  }
})