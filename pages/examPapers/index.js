//index.js
//获取应用实例
const app = getApp()
const jinrishici = require('../../utils/jinrishici.js')
Page({
  data: {
    spinShow: false,
    fixedPaperCount: 0,
    taskPaperCount: 0,
    timeLimitPaperCount: 0,
    pushPaperCount: 0,
    shici: '长风破浪会有时，直挂云帆济沧海。',
    fixedPaper: [],
    pushPaper: [],
    timeLimitPaper: [],
    taskList: [],
    msgList: []
  },
  onLoad: function () {
    this.setData({
      spinShow: true
    });
    this.indexLoad()
    app.formPost('/api/wx/student/user/current', null).then(res => {
      if (res.code == 1) {
        app.globalData.userInfo = res.response
      }
    }).catch(e => {
      app.message(e, 'error')
    })
  },

  onShareAppMessage: function (res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: '在线考试exam',
      path: '/page/index/index'
    }
  },
  onShareTimeline: function (t) {
    return {
      title: "在线考试exam",
      query: "",
      imageUrl: ""
    }
  },
  onPullDownRefresh() {
    this.setData({
      spinShow: true
    });
    if (!this.loading) {
      this.indexLoad()
    }
  },
  indexLoad: function () {
    let _this = this
    app.formPost('/api/wx/student/dashboard/index', null).then(res => {
      _this.setData({
        spinShow: false
      });
      wx.stopPullDownRefresh()
      if (res.code === 1) {
        _this.setData({
          fixedPaper: res.response.fixedPaper,
          timeLimitPaper: res.response.timeLimitPaper,
          pushPaper: res.response.pushPaper,
          msgList: res.response.msgList,
          fixedPaperCount: res.response.fixedPaper.length,
          timeLimitPaperCount: res.response.timeLimitPaper.length
        });
        app.globalData.fixedPaper= res.response.fixedPaper
        app.globalData.timeLimitPaper= res.response.timeLimitPaper
        app.globalData.pushPaper= res.response.pushPaper
        app.globalData.fixedPaperCount= res.response.fixedPaper.length,
        app.globalData.timeLimitPaperCount= res.response.timeLimitPaper.length
        if( res.response.pushPaper!=null){
          _this.setData({
            pushPaperCount: res.response.pushPaper.length
          })
          app.globalData.pushPaperCount= res.response.pushPaper.length
        }
       
       
      }
    }).catch(e => {
      _this.setData({
        spinShow: false
      });
      // app.message(e, 'error')
    })

    app.formPost('/api/wx/student/dashboard/task', null).then(res => {
      _this.setData({
        spinShow: false
      });
      wx.stopPullDownRefresh()
      if (res.code === 1) {
        _this.setData({
          taskList: res.response,
          taskPaperCount: res.response.length
        });
        app.globalData.taskList= res.response
        app.globalData.taskPaperCount= res.response.length
      }
    }).catch(e => {
      _this.setData({
        spinShow: false
      });
      // app.message(e, 'error')
    })
    // 每日诗词
    jinrishici.load(result => {
      app.globalData.jinrishici1 = result.data.content
      // 下面是处理逻辑示例
      _this.setData({
        "jinrishici": result.data.content,
        shici: app.globalData.jinrishici1
      });

      console.log(app.globalData.jinrishici1)
    })
  },


})