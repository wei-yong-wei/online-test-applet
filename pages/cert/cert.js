// pages/school/cert.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isLoading: true,
    userInfo:null,
    modalName:null,
    neirong:null,
    hasWeappMethod: ['http://cet.neea.edu.cn/cet/'],
    cardColor: ['red', 'orange', 'yellow', 'olive', 'green', 'blue', 'purple', 'mauve', 'pink'],
    certList: [{
        title: '水联网硬件产品报价V4.2',
        hookId: 'pdf',
        url: "https://6461-dabenben-jrvrn-1300136165.tcb.qcloud.la/%E6%B0%B4%E8%81%94%E7%BD%91%E7%A1%AC%E4%BB%B6%E4%BA%A7%E5%93%81%E6%8A%A5%E4%BB%B7v4.2-(2021.3.8)PDF%E7%89%88.pdf?sign=6102d63fe3bdf407081627ed518e95b2&t=1621228807"
      }, {
        title: '水联网软件产品报价V4.2',
        hookId: 'pdf',
        url: "https://6461-dabenben-jrvrn-1300136165.tcb.qcloud.la/%E6%B0%B4%E8%81%94%E7%BD%91%E8%BD%AF%E4%BB%B6%E4%BA%A7%E5%93%81%E6%8A%A5%E4%BB%B7v4.2-(2021.3.8).pdf?sign=cbc79c5b2be336b076339314f4da2609&t=1621228822"
      },
      {
        title: '水联网漏损系统本地部署服务器报价V4.2',
        hookId: 'pdf',
        url: "https://6461-dabenben-jrvrn-1300136165.tcb.qcloud.la/%E6%B0%B4%E8%81%94%E7%BD%91%E6%BC%8F%E6%8D%9F%E7%B3%BB%E7%BB%9F%E6%9C%AC%E5%9C%B0%E9%83%A8%E7%BD%B2%E6%9C%8D%E5%8A%A1%E5%99%A8%E4%BA%A7%E5%93%81%E6%8A%A5%E4%BB%B7v4.2-(2021.3.8)PDF%E7%89%88.pdf?sign=541497c5fbfb86c3ed940fea05a5613c&t=1621228833"
      },
    ],
    certListOne: [
  ],
  certList1: [{
    title: '二次供水平台实施硬件及价格V1.7',
    hookId: 'xlsx',
    url: "https://6461-dabenben-jrvrn-1300136165.tcb.qcloud.la/%E4%BA%8C%E6%AC%A1%E4%BE%9B%E6%B0%B4%E5%B9%B3%E5%8F%B0%E5%AE%9E%E6%96%BD%E7%A1%AC%E4%BB%B6%E5%8F%8A%E4%BB%B7%E6%A0%BCV1.7.xlsx?sign=5c92314cdf788398836da721a8ee1fce&t=1621467960"
  }
]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      userInfo:app.globalData.userInfo
    })
    this.inital();
  },
  inital: function () {
    wx.showShareMenu({
      withShareTicket: true,
      menus: ['shareAppMessage', 'shareTimeline']
    })
  },

  showModal(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },
  goToHook(e) {
    var that = this;
    const url = e.currentTarget.dataset.target
    console.log(url)
      that.setData({
        modalName:"Modal",
        neirong: url
      });
      wx.setClipboardData({
        data: url,
        success(res) {
          wx.getClipboardData({
            success(res) {
              console.log(res.data) // data
              wx.showToast({
                title: '复制成功,粘贴到浏览器访问',
                icon: 'none'
              })
            }
          })
        }
      })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    var that = this;
    setTimeout(function () {
      that.setData({
        isLoading: false
      });
    }, 400);
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: '“在线考试” - 产品报价',
      path: 'pages/cert/cert'
    }
  },
})