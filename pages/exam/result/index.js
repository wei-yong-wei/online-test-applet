let app = getApp()
Page({
    data: {
        title: null,
        percent: null,
        isPass:"本次考试不及格，请再接再厉！",
        tishi:"有志者事竟成，破釜沉舟，百二秦川终属楚！",
        time: null,
        paperScore: null,
        questionCount: 0,
        questionCorrect: 0,
        address: null,
        id: 0,
        percent: 0,
        info: {},
        userScore:null,
        userScore1:null
    },
    onLoad: function (options) {
        let id = options.id
        // console.log("传过来的id是"+id)     
        let _this = this
        _this.setData({
            spinShow: true,
            id:  options.id,
            info: app.globalData.userInfo
        });
        app.formPost('/api/wx/student/exampaper/answer/result/' +_this.data.id , null)
            .then(res => {
                _this.setData({
                    spinShow: false
                });
                if (res.code === 1) {
                    _this.setData({
                        // id:res.response.id,
                        title: res.response.paperName,
                        userScore: res.response.userScore,
                        time: res.response.doTime,
                        paperScore: res.response.paperScore,
                        questionCount: res.response.questionCount,
                        questionCorrect: res.response.questionCorrect,
                        address: res.response.address,
                        percent: (res.response.questionCorrect / res.response.questionCount * 100).toFixed(1)
                    });
                    if(res.response.isLock=="否"){
                        _this.setData({
                                userScore:"你猜猜得多少分！"
                        })
                    };
                    if(_this.data.userScore>=_this.data.paperScore*0.6){
                        _this.setData({
                            isPass:"恭喜你，本次考试及格！",
                            tishi:"苦心人天不负，卧薪尝胆，三千越甲可吞吴！"
                        })
                    }
                }
            }).catch(e => {
                _this.setData({
                    spinShow: false
                });
                app.message(e, 'error')
            })
    },
    onShow: function () {},
    onShareAppMessage: function () {
    },
    toAnswer: function () {
        wx.reLaunch({
            url: "/pages/record/index"
        });
    },
    toIndex: function () {
        wx.reLaunch({
            url: "/pages/index/index"
        });
    },
    toListDetail: function (t) {
    },
    showMessageDialog: function () {
        wx.requestSubscribeMessage({
            tmplIds: ['Q5B4N5l0cmyoNYC7siPyF7rv-xN6aT8FV3jWqUCRQ2Y', 'MPdYYz2g1BlAWRyg9bUOiOZwS6Q0l_1Ag48OyM6F1bs', 'Ys-DSceVKE9EdxlkZgUceNNbikobYAntZ_weRNvG0qE'],
            success(res) {
              console.log("授权成功！")
              wx.showToast({
                title: '订阅成功！',
                icon: 'success',
                duration: 2000 //持续的时间
              })
              wx.vibrateShort({
                type: 'heavy'
              })
              wx.navigateTo({
                url: "/pages/index/index"
            });
            },
            fail(res) {
              wx.showToast({
                title: '订阅失败！！！',
                icon: 'error',
                duration: 2000 //持续的时间
              })
            }
          })
    },
});