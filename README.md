# 在线考试小程序

## 介绍
在线考试小程序前端代码，全部开源！

基于gitee上的exam进行二次开发，小程序端使用了colour-UI进行重新页面设计，集成腾讯地图、echarts等插件，进行成绩分析排名。
后端也是经过二次定制化开发，集成公司、部门权限管理；题目导入导出；随机试卷等一系列功能。
同时集成微信订阅消息、定时任务等。可在考试前一小时提前发送订阅消息提醒，考试完成后发送分数提醒，试卷公开后发送提醒等。

## 联系

目前小程序已经发布，大家可以体验下，在学习的过程中遇到问题，可以咨询我 (备注好来自码云)

## 适用场景
     1 ·企业招聘考试。候选人等候时面试可手机扫码参加笔试，系统实时生成结果，一个简单的考核就可让面试官提前了解候选人的能力
     情况。在大规模校园聘会上公布考试地址，考生现场完成考试，现场出分，主办方可快速筛选合格考生参加面试，大大缩短招聘周期和成本。

     2 ·员工晋升考核。员工专业技能是否达标，员工素质测评，安排一场考试即可对员工是否晋升进行有理的判断。全面电子化式考试，
     可大大缩短员工考核周期与成本。

     3 ·培训认证考试。培训机构结业认证考核，行业从业资格认证，能力认证考核等，题答答提供从考生报名到参加考试的全流程功能。
     无限制题库刷题，错题训练，随机题序，答案解析，错题本重练，让刷题不止追求量，也有质的保障，帮助考生更熟练地掌握知识点，
     提高考试通过率。


## 体验账号

测试账号，密码（线上版本，请勿修改密码，谢谢！）
账号：student
密码：123456

## 在线体验小程序

![微信扫码体验](https://images.gitee.com/uploads/images/2021/0124/164331_04674cc5_4927128.png "屏幕截图.png")

## 小程序功能截图
<img src = "https://images.gitee.com/uploads/images/2021/0708/155634_fd4e5b69_4927128.png" width = "30%">
<img src = "https://images.gitee.com/uploads/images/2021/0708/155739_0a50bf06_4927128.png" width = "30%">
<img src = "https://images.gitee.com/uploads/images/2021/0708/155838_8c25f532_4927128.png" width = "30%">
<img src = "https://images.gitee.com/uploads/images/2021/0708/155922_b69644c3_4927128.png" width = "30%">
<img src = "https://images.gitee.com/uploads/images/2021/0708/155954_57f82e2b_4927128.png" width = "30%">
<img src = "https://images.gitee.com/uploads/images/2021/0708/160030_19802846_4927128.png" width = "30%">
<img src = "https://images.gitee.com/uploads/images/2021/0708/160053_2af4396a_4927128.png" width = "30%">
<img src = "https://images.gitee.com/uploads/images/2021/0708/160124_6799d883_4927128.png" width = "30%">
<img src = "https://images.gitee.com/uploads/images/2021/0708/160149_262b76a7_4927128.png" width = "30%">
<img src = "https://images.gitee.com/uploads/images/2021/0708/160212_5fc84d56_4927128.png" width = "30%">
<img src = "https://images.gitee.com/uploads/images/2021/0708/160232_4f70b6f7_4927128.png" width = "30%">
<img src = "https://images.gitee.com/uploads/images/2021/0708/160252_9cb506f7_4927128.png" width = "30%">
<img src = "https://images.gitee.com/uploads/images/2021/0708/160311_d7417b79_4927128.png" width = "30%">
<img src = "https://images.gitee.com/uploads/images/2021/0708/160333_4b5ab20c_4927128.png" width = "30%">
<img src = "https://images.gitee.com/uploads/images/2021/0708/160351_5dc4ddcc_4927128.png" width = "30%">
<img src = "https://images.gitee.com/uploads/images/2021/0708/160408_40f8624d_4927128.png" width = "30%">
<img src = "https://images.gitee.com/uploads/images/2021/0708/160431_6823c361_4927128.png" width = "30%">
<img src = "https://images.gitee.com/uploads/images/2021/0708/160450_6cd26029_4927128.png" width = "30%">
<img src = "https://images.gitee.com/uploads/images/2021/0708/160510_e2ebf331_4927128.png" width = "30%">
<img src = "https://images.gitee.com/uploads/images/2021/0708/160527_7779c248_4927128.png" width = "30%">
<img src = "https://images.gitee.com/uploads/images/2021/0708/160557_ebe27473_4927128.png" width = "30%">
<img src = "https://images.gitee.com/uploads/images/2021/0708/160616_784f3e57_4927128.png" width = "30%">
<img src = "https://images.gitee.com/uploads/images/2021/0708/160636_f11c2a83_4927128.png" width = "30%">
<img src = "https://images.gitee.com/uploads/images/2021/0708/160701_99fee567_4927128.png" width = "30%">
<img src = "https://images.gitee.com/uploads/images/2021/0708/160724_dddb088b_4927128.png" width = "30%">
<img src = "https://images.gitee.com/uploads/images/2021/0708/160745_84ff8ac2_4927128.png" width = "30%">
<img src = "https://images.gitee.com/uploads/images/2021/0708/160812_aee159d3_4927128.png" width = "30%">
<img src = "https://images.gitee.com/uploads/images/2021/0708/160848_1866a7c8_4927128.png" width = "30%">
<img src = "https://images.gitee.com/uploads/images/2021/0708/160907_17a238f5_4927128.png" width = "30%">
<img src = "https://images.gitee.com/uploads/images/2021/0708/160930_7783d982_4927128.png" width = "30%">

## 特点
请扫码体验，截图只截取了部分功能，大量功能没有截取到
+ 完整的考试系统，支持随机试题、加密试题、时段试题、班级试题、固定试题、任务试题
+ 正在开发中的调研问卷系统，满足日常调研需求
+ 知识分享系统，包含各种知识资料文件分享功能
+ 百问百答系统，解决日常简单问答需求
+ 考试分析系统，支持图表、排名、统计等分析功能。可以设置试卷是否公开、是否允许学员看分数。
+ 班级管理体系，支持多班级管理
+ 证书体系，支持及格生成证书
+ 通知体系，支持考试前通知，考完通知，成绩公布通知。基于微信订阅消息的通知体系
+ 各种题型体系，支持单选、多选、填空、问答、图片等多种形式题目，兼容数学公式
+ 开放式注册体系，支持注册验证，审核
+ 位置记录功能，支持记录考生位置信息，实时分析
+ 个人画像功能，支持根据个人使用记录进行实时分析
+ 全局搜索功能，可以实时搜索所有资料信息
+ 个性化推送功能，可以自定义推送图文信息

## 下一步开发计划
+ 错题集
+ 问卷调研完善
+ 知识分享系统完善，加入文档简介
+ 考试分析系统完善，加入图表分析

